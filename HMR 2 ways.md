# 设置 HMR 的2种方式
目录 {#index}
跳转到[目录](#index)

## [巨详细文档说明](https://github.com/peter-mouland/react-lego#importing-css)


与 [`redbox-react`](https://github.com/commissure/redbox-react) 一起讲.
## 1. 利用 [react-hot-loader 3](https://github.com/gaearon/react-hot-loader)

### 与`redbox-react`整合的例子
The react-hot-loader [example][1.1] features the upcoming version 3 (currently 3.0.0-beta.2), but does not yet support error catching and rendering on updates, only on initial mount. This is the future, but it's not quite here.

[1.1]:https://github.com/commissure/redbox-react/blob/master/examples/react-hot-loader

### 官方文档[Hot Module Replacement - React][1.2]

[1.2]:https://webpack.js.org/guides/hmr-react/

### 注意-1
webpack-dev-server 必须开启 2 种 hot server!!!!
- 一个 由 属性 `devServer.hot: true` 控制(即`entry: 'webpack/hot/dev-server'`)  使用 `dev-server.js`.
- 另一个 由 `entry: 'webpack/hot/only-dev-server'` 控制 使用 `only-dev-server.js`

加上 `react-hot-loader`  的 'react-hot-loader/patch' 就是3个。

### 注意-2
若 `devServer.publicPath` 有设置，则其值 **必须**也建议 与 `output.publicPath`  一样。
若不一样，则
1. 与 `index.html` 中的`bundle.js`的引入地址 `src="/build/bundle.js"` 中的 `/build/`部分必须保持一致，
   否则网页会找不到`bundle.js`，因为此时的正确地址为：`devServer.publicPath` + bundle.js。
   也就是说，`devServer.publicPath`覆盖了`output.publicPath`。
   - 1.1. **但这还是会导致再热加载时，本应该`局部替换页面不刷新`的情况变成`页面整体刷新`!!!**
          因为 `output.publicPath` // necessary for HMR to know where to load the hot update chunks

#### 导致再热加载时，本应该`局部替换页面不刷新`的情况变成`页面整体刷新` 的失误配置点
1. 在正确设置好**局部替换页面不刷新热加载**相关配置后，又去设置了 `devServer.watchContentBase = true`。
2. `index.html` 中的`bundle.js`的引入地址 `src="/build/bundle.js"` 中的 `/build/`部分 与 `output.publicPath` 值不一致。
3. `devServer.hot` 没有设置为 `true`。

### 配置注意事项
```js
//.babelrc
"presets": [
  ["latest", { // 注意这里的书写格式
    "es2015": {
      "loose": true,
      "modules": false
    }
  }],
  "stage-1",
  "react"
],
// 在配置文件没做好 分环境 考虑设置时，这个必须
"plugins": ["react-hot-loader/babel"],
// 用于已分好 开发环境用
"env": {
  "development": {
    "plugins": [
      "react-hot-loader/babel"
    ]
}
}
```

```js
const render = (Component) => {
  ReactDOM.render(
    <AppContainer errorReporter={Redbox}>
      <Component />
    </AppContainer>,
    // document.querySelector('#container'),
    document.getElementById('container'),
  )
}

render(App) // 不能省

if (module.hot) {
  console.log(module.hot)
  // module.hot.accept('components/App', () => {
  module.hot.accept((err) => {  // 地址参数可以省去
    if (err) {
      console.error('Cannot apply hot update', err)
    }
    render(App)
  })
}
```
```js
devServer: {
  hot: true, // 使用'webpack/hot/dev-server'
  // watchContentBase: true, // 会引起整个页面刷新
  // 这项可有可无
  watchOptions: {
    aggregateTimeout: 300, // rebuild 延时, wait so long for more changes
    ignored: /node_modules/,
    poll: 1000, // 设置检测文件变化的间隔时间段，Check for changes every second
  },
}
```
## 2. 利用 [babel-plugin-react-transform](https://github.com/gaearon/babel-plugin-react-transform) - **不推荐**

### 与`redbox-react`整合的例子
The [react-transform-catch-errors][2.1] [example][2.2] shows how to catch and render errors with the deprecated react-transform-catch-errors plugin. This is the way of the past, but it works today.

[2.1]:https://github.com/gaearon/react-transform-catch-errors
[2.2]:https://github.com/commissure/redbox-react/blob/master/examples/react-transform-catch-errors

## 添加React Transform支持 https://zhuanlan.zhihu.com/p/20522487


 `Babel-plugin-react-transform` 已经过时了，开发者也宣布已经停止维护
```js
 "env": {
   "development": {
     "presets": [
       "react-hmre"
     ],
   "plugins": [
     "react-hot-loader/babel",
       ["react-transform", {
           "transforms": [{
             "transform": "react-transform-hmr",
             "imports": ["react"],
             "locals": ["module"]
             }, {
             "transform": "react-transform-catch-errors",
             "imports": ["react", "redbox-react", "../reporterOptions.js"]
           }],
           "factoryMethods": ["React.createClass", "createClass"]
     }]
   ]
 }
 }
 ```
