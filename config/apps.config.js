const path = require('path')
const { APP_PATH, PUBLIC_PATH } = require('./webpack/paths')
const { commonServer } = require('./webpack/devServer')
// 模板网页
const defaultIndexPage = './../src/index.html'

module.exports = {
  currentAppEntrySrc: './main.js', // 当前使用的App项目名字+Entry File Name
  // 基本的应用配置信息
  apps: [
    // APP-1
    {
      id: 'helloworld',
      src: path.join(APP_PATH, 'main.js'), // './src/app名字/index.js',
      indexPage: defaultIndexPage,
      compiled: false,
    },
    // APP-2
  ],

  // webpack.resolve
  resolve: {
    extensions: ['.js', '.jsx', '.json'], // '' 移给enforceExtension配置
    enforceExtension: false, // true : 不匹配 ''
    modules: [APP_PATH, 'node_modules'],
    // 设置路径别名
    alias: {
      js: path.join(APP_PATH, 'scripts'),
      components: path.join(APP_PATH, 'components'),
      styles: path.join(APP_PATH, 'styles'),
      img: path.join(APP_PATH, 'img'),
    },
    // plugins: [new DirectoryNamedWebpackPlugin()],
  },

  // 开发服务器配置
  devServer: Object.assign(commonServer, {
    // -----------------------添加 webpack-dev-server 特有属性
    hot: true, // 使用'webpack/hot/dev-server'
    // 开始目录基准，默认是项目目录 可以是数组[], 或 false (禁用)
    contentBase: APP_PATH, // './src',  // 8080/index.html
    port: 9000,
    // It's a required option.
    // 建议devServer.publicPath 与 output.publicPath 保持一致
    // src="bundle.js" => src="build/bundle.js"
    publicPath: PUBLIC_PATH,
    // proxy: {}
    // -----------------------添加 express 特有属性
    // reporter: null,
    // Provide a custom reporter to change the way how logs are shown.

    // serverSideRender: false,
    // Turn off the server-side rendering mode. See Server-Side Rendering part for more info.
  }),

  // 用于服务端渲染的Server路径
  ssrServer: {
    serverEntrySrc: '',
  },

  // 依赖项配置
  proxy: {
    // 后端服务器地址 http://your.backend/
    backend: '',
  },

  // 如果是生成的依赖库的配置项
  library: {
    name: 'library_portal', // 依赖项入口名
    entry: './src/app名字/library/library_portal.js', // 依赖库的入口,
    libraryName: 'libraryName', // 生成的挂载在全局依赖项下面的名称
    libraryTarget: 'var', // 挂载的全局变量名 或 umd
  },
}
