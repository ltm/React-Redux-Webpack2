
// 定义非直接引用依赖
// 定义第三方直接用Script引入而不需要打包的类库
// 使用方式即为var $ = require("jquery")
const externals = {
  window: 'window',
  pageResponse: 'pageResponse',
}


// PostCSS plugins
// autoprefixer

// 使用postcss作为默认的CSS编译器
const postCSSConfig = [
]

module.exports = { externals, postCSSConfig }
