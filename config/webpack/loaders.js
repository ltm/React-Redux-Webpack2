const { APP_PATH, NODE_ENV } = require('./paths')

// 基于Babel的JS/JSX Loader
const jsx = {
  test: /\.(js[x]?)$/,
  exclude: /(node_modules)/,
  include: APP_PATH,
  use: ['babel-loader'],
}

// 对于TS与TSX的Loader

// 根据不同的环境开发设置不同的样式加载的Loader

// 如果当前为编译环境,则抽取出CSS代码
let style
if (NODE_ENV === 'development') {
  // 如果当前为开发环境,则封装内联的CSS
  style = {
  }
} else {
  style = {
  }
}

// 对于图片与字体文件的导入工具,并且设置默认的dist中存放方式
// inline base64 URLs for <=8k images, direct URLs for the rest
const assets = {
}

// 对于JSON文件的导入
const json = {
}

module.exports = { jsx, style, json, assets }
