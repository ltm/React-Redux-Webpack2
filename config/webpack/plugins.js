/*
eslint
  import/no-extraneous-dependencies: ["error", {"devDependencies": true}],
  global-require:"warn",
  no-underscore-dangle: ["error", { "allow": ["__DEV__"] }]
*/
const webpack = require('webpack')

const { NODE_ENV, __DEV__ } = require('./paths')


// 通用插件组
const commonPlugins = [

  // 定义环境变量
  new webpack.DefinePlugin({
    'process.env': {
      // 因为使用热加载，所以在开发状态下可能传入的环境变量为空
      NODE_ENV: JSON.stringify(NODE_ENV),
    },
    // 判断当前是否处于开发状态
    __DEV__: JSON.stringify(__DEV__),
    __SERVER__: JSON.stringify(false),
  }),
]


// 开发时使用插件
const devPlugins = [
  new webpack.HotModuleReplacementPlugin(),
  new webpack.NamedModulesPlugin(),
  // prints more readable module names in the browser console on HMR updates
  new webpack.NoEmitOnErrorsPlugin(),
  new webpack.LoaderOptionsPlugin({
    options: {
      context: '/',
    },
  }),
]

// 判断是否为开发模式,如果为开发模式则添加WebpackDashboard
if (__DEV__) {
  /* eslint-disable-line  */
  const DashboardPlugin = require('webpack-dashboard/plugin')
  devPlugins.push(new DashboardPlugin({ color: 'magenta' }))
}


// 生产环境下使用插件
const prodPlugins = [

  // 提取所有Loaders相同定义到同一地方
  new webpack.LoaderOptionsPlugin({
    // test: /\.css$/, // 可以传入 test、include 和 exclude，默认会影响所有的 loader
    minimize: true,
    debug: false,
    options: {
      context: '/',
    },
  }),

  new webpack.optimize.AggressiveMergingPlugin(), // Merge chunks
]

module.exports = { commonPlugins, prodPlugins, devPlugins }
