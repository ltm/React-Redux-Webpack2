/*
eslint
  no-underscore-dangle: ["error", { "allow": ["__DEV__"] }]
*/
const { PUBLIC_PATH, APP_PATH, BUILD_PATH, __DEV__, NODE_ENV } = require('./webpack/paths')
const { jsx } = require('./webpack/loaders') // loaders
const { commonPlugins, devPlugins, prodPlugins } = require('./webpack/plugins')
const utils = require('./webpack/utils')
const appsConfig = require('./apps.config')

// 定义入口变量
let entry

// 根据不同的环境状态设置不同的开发变量
if (__DEV__) {
  // 开发状态下的默认入口
  entry = [
    'react-hot-loader/patch',
    // `webpack-dev-server/client?http://0.0.0.0:${appsConfig.devServer.port}`,
    'webpack/hot/only-dev-server',
    appsConfig.currentAppEntrySrc,
  ]
} else if (NODE_ENV === 'library') {
  // 配置依赖库性质的编译环境
  entry = [appsConfig.library.entry]
} else {
  entry = {
    vendors: './dev-config/vendors.js', // 存放所有的公共文件
  }
}

// 设置开发时源代码映射工具
// devtool 指明了sourcemap的生成方式，它有七个选项，具体请参考 https://segmentfault.com/a/1190000004280859
// sourcemap 的作用就是为调试代码提供便利
// cheap-module-eval-source-map 绝大多数情况下都会是最好的选择，这也是下版本 webpack 的默认选项。
const devTool = __DEV__ ? 'cheap-module-eval-source-map' : 'hidden-source-map'

// 基本配置
const config = {
  // 设置包含 entry 文件的目录的绝对路径
  // context 会影响到 ./src/main.js 的url
  context: APP_PATH,  // ./src/main.js => ./main.js

  cache: false,
  entry,
  devtool: devTool,
  // 所有的出口文件，注意，所有的包括图片等本机被放置到了build目录下，其他文件放置到static目录下
  output: {
    path: BUILD_PATH, // './build',
    // necessary for HMR to know where to load the hot update chunks
    publicPath: PUBLIC_PATH, // '/build/',
    filename: 'bundle.js',
    // filename: '[name].bundle.js', // 文件名,不加chunkhash,以方便调试时使用
    // sourceMapFilename: '[name].bundle.map', // 映射名
    // chunkFilename: '[name].[chunkhash].chunk.js', //块文件索引
  },
  // 配置插件
  plugins: __DEV__ ?
    // 开发环境下所需要的插件
    [].concat(commonPlugins).concat(devPlugins) :
    // 生产环境下需要添加的插件
    [].concat(commonPlugins).concat(prodPlugins),
  module: {
    noParse: [/\.min\.js/, // /jquery|lodash/,
    ],
    rules: [jsx,
    ],
  },
  externals: utils.externals,
  // resolve: appsConfig.resolve,
  resolve: {
    extensions: ['.js', '.jsx', '.json'], // '' 移给enforceExtension配置
    enforceExtension: false, // true : 不匹配 ''
    modules: [APP_PATH, 'node_modules'],
    // 设置路径别名
    alias: {
      components: APP_PATH + '/components',
    },
    // plugins: [new DirectoryNamedWebpackPlugin()],
  },
  devServer: appsConfig.devServer,
}

// 如果当前是Library配置
if (NODE_ENV === 'library') {
  const library = appsConfig.library

  // 添加生成项的依赖库名
  config.output.library = library.libraryName

  // 添加全局挂载名
  config.output.libraryTarget = library.libraryTarget

  // 如果是生成环境下，将文件名加上hash
  config.output.filename = `${library.name}.library.js`
}

// 为生产环境添加额外配置

module.exports = config
