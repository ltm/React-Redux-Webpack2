import React, { Component } from 'react'
// import styles from './App.css'

// export default class App extends Component {
//   render() {
//     return (
//       <div className={styles.app}>
//         <h2>Hello, {this.props()}</h2>
//       </div>)
//   }
// }
class Counter extends Component {
  constructor(props) {
    super(props)
    this.state = { counter: 0 }
    this.interval = setInterval(() => this.tick(), 1000)
  }

  componentWillUnmount() {
    clearInterval(this.interval)
  }

  tick() {
    this.setState({
      counter: this.state.counter + this.props.increment,
    })
  }

  render() {
    return (
      <h1 style={{ color: this.props.myColor }}>
        Counter ({this.props.increment}): {this.state.counter}
      </h1>
    )
  }
}

const App = () => (
  // <div className={styles.app}>
  <div>
    <h3>Hello, React hot1!</h3>
    <Counter increment={10} myColor="red" />
  </div>
)

export default App
