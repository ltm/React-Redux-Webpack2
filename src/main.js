import React from 'react'
import ReactDOM from 'react-dom'
import { AppContainer } from 'react-hot-loader'
import Redbox from 'redbox-react'
import App from 'components/App'

const render = (Component) => {
  ReactDOM.render(
    <AppContainer errorReporter={Redbox}>
      <Component />
    </AppContainer>,
    // document.querySelector('#container'),
    document.getElementById('container'),
  )
}

render(App) // 不能省

if (module.hot) {
  console.log(module.hot)
  // module.hot.accept('components/App', () => {
  module.hot.accept((err) => {
    if (err) {
      console.error('Cannot apply hot update', err)
    }
    render(App)
  })
}
