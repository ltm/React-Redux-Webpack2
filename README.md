#React-Redux-Webpack2

## 目标
1. 单一的配置文件：很多项目里面是把开发环境与生产环境写了两个配置文件，但笔者考虑到两种不同环境存在许多相同的配置内容，而且不同部分可以做成通过传入特定环境相关参数来让webpack动态选择对应环境的对应配置内容，因此笔者就索性把它归纳为单一的配置文件特性，然后通过npm封装不同的编译命令传入环境变量，然后在配置文件中根据不同的环境变量进行动态响应。另外，要保证一个Boilerplate能够在最小修改的情况下应用到其他项目。

2. 多应用入口支持：无论是单页应用还是多页应用，在Webpack中往往会把一个html文件作为一个入口。
在进行项目开发时，可能会需要面对多个入口（即面向应用方式的封装配置），但笔者暂时只用到单个入口，即Webpack原生提倡的配置方案是面向过程的。为了适应性，决定整理出这么一个特性（暂时可能不会实现）。

3. 调试时热加载：这个特性毋庸多言，不过热加载因为走得是中间服务器，同时只能支持监听一个项目，因此需要在多应用配置的情况下加上一个参数，即指定当前调试的应用。

4. 调试时错误信息捕捉并高亮显示到页面：

5. 自动化的Polyfill：通过整合Webpack自带的这个特性，实现了对于ES6、React、CSS(Flexbox)等等的自动Polyfill。

6. 资源文件的自动管理：这部分主要指从模板自动生成目标HTML文件、自动处理图片/字体等静态资源文件以及自动提取出CSS文件等。

7. 文件分割与异步加载：可以将多个应用中的公共文件，譬如都引用了React类库的话，可以将这部分文件提取出来，这样前端可以减少一定的数据传输。另外的话还需要支持组件的异步加载，譬如用了React Router，那需要支持组件在需要时再加载。

参考 [在重构脚手架中掌握React/Redux/Webpack2基本套路](https://segmentfault.com/a/1190000007166607?hmsr=toutiao.io&utm_medium=toutiao.io&utm_source=toutiao.io#articleHeader0)
## 本模板有两种模式：

1. 开发模式————有热替换功能，可以将错误信息捕捉并高亮显示到页面。

2. 生产模式————将你的资源进行打包，压缩等。

## 核心摘要

- [x] [Webpack](https://webpack.github.io)
- [x] [React](https://facebook.github.io/react/)
- [x] [Babel](https://babeljs.io/)

## 启动 开发模式

```
$ npm start
```

## 启动 生产模式（打包输出）

```
$ npm run build
```

## 本模板用到的 __所有包__ 的相关简要说明

####[react.js](https://facebook.github.io/react/index.html) [必需]
> React是用来构建用户界面的js库，属于view层。
  它有两大特点：1，单向数据绑定；2，虚拟DOM
  安装：`npm install --save react`

---

####[react-dom.js](https://www.npmjs.com/package/react-dom) [必需]
> react.js 主要用来创建元素和组件，当你想在html中渲染你的组件的时候，
你还得需要react-dom.js。同时，react-dom.js依赖于react.js。
安装：`npm install --save react-dom`

---

####[webpack](https://www.npmjs.com/package/react-dom) [必需]
> 于人而言，尤其是当开发大型项目时，每个包每个模块每个静态资源都应尽可能的条理清晰的罗列出来，
这样方便我们开发；于机器而言，就不需要这么“条理清晰”了，此时应最大限度的压缩优化这些资源，
如何把这些资源模块“杂糅”在一起，这就是webpack要做的。
安装：`npm install --save-dev webpack`
备注：webpack 2.0 即将发布
webpack 最基本的启动webpack命令
webpack -w 提供watch方法，实时进行打包更新
webpack -p 压缩混淆脚本，这个非常非常重要！
webpack -d 生成map映射文件，告知哪些模块被最终打包到哪里了,方便调试
webpack --progress 显示打包进程，百分比显示
webpack --config XXX.js //使用另一份配置文件（比如webpack.config2.js）来打包
webpack --colors 输出结果带彩色，比如：会用红色显示耗时较长的步骤
webpack --profile 输出性能数据，可以看到每一步的耗时
webpack  --display-error-details 方便出错时能查阅更详尽的信息（比如 webpack 寻找模块的过程），从而更好定位到问题。
webpack --display-modules 默认情况下 node_modules 下的模块会被隐藏，加上这个参数可以显示这些被隐藏的模块
[webpack入门配置](https://segmentfault.com/a/1190000005089993)

---

####[webpack-dashboard](https://www.npmjs.com/package/webpack-dashboard) [开发需要]
> 它是一款命令行界面仪表盘工具，用于WebPack开发服务端。
用于改善开发人员使用webpack时控制台用户体验的一款工具。它摒弃了webpack（尤其是使用dev server时）在命令行内诸多杂乱的信息结构，为webpack在命令行上构建提供了一目了然的仪表盘(dashboard)，其中包括 构建过程 和 状态 、 日志 以及涉及的 模块列表 。使用它，你就可以更加优雅的使用webpack构建你的代码。
安装：
```bash
npm install webpack-dashboard --save-dev
```

Options
- -c, --color [color] - Custom ANSI color for your dashboard
- -m, --minimal - Runs the dashboard in minimal mode
- -t, --title [title] - Set title of terminal window
- -p, --port [port] - Custom port for socket communication

##### server: webpack-dev-server
直接在 webpack.config.js 里面初始化dashboard。
```js
// 首先，导入dashboard和其对应的插件，并创建一个dashboard的实例：
// const Dashboard = require('webpack-dashboard')
const DashboardPlugin = require('webpack-dashboard/plugin')
// const dashboard = new Dashboard()
// 然后，在对应的 plugins 里面添加DashboardPlugin：
plugins: [
  // new DashboardPlugin(dashboard.setData),
  new DashboardPlugin(), // 简化
  // new DashboardPlugin({ port: '9001', color: 'magenta' }),
]
```
```js
"scripts": {
    "dev": "webpack-dashboard -- webpack-dev-server --config ./webpack.dev.js"
}
```
##### server: express
未整理，可查找官方文档

---

####[webpack-dev-server](https://www.npmjs.com/package/webpack-dev-server) [开发需要]
> 它是一个静态资源服务器，只用于开发环境，实现开发流程中的自动刷新。
webpack-dev-server是一个小型的Node.js Express服务器,它使用webpack-dev-middleware来服务于webpack的包,除此自外，它还有一个通过Sock.js来连接到服务器的微型运行时.
和直接在命令行里运行webpack不同的是，webpack-dev-server会把编译后的静态文件全部保存在内存里，而不会写入到文件目录内。
我们的页面如何在这个服务器上更新呢，首先是取得webpack打包好的资源，这就需要在`请求`到`响应`的过程中通过
express的中间件取得资料， 而方法就是通过webpack-dev-middleware来实现。
这个中间件只在开发环境中使用，切忌用在生产环境中。

安装：`npm install --save-dev webpack-dev-server`

####这个中间件有3点好处：

1. 直接在内存中操作文件，而非磁盘中。这样处理速度更快。
2. 在监视（watch）模式下，如果文件改变，中间件立即停止提供之前的bundle，并且会延迟请求回应，直到新的编译完成，如此一来，文件修改后，你可以直接刷新页面，而不用等待编译。
3. 热替换功能与自动刷新

---

####[webpack-dev-middleware](https://www.npmjs.com/package/webpack-dev-middleware) [开发需要-被webpack-dev-server替代]

---

####[webpack-hot-middleware](https://www.npmjs.com/package/webpack-hot-middleware) [开发需要-被webpack-dev-server替代]
---

####[babel-core](https://www.npmjs.com/package/babel-core) [必需]
> Babel是一个转换编译器，它能将ES6转换成可以在浏览器中运行的代码。
作为下一代javascript语言标准，请拥抱ES6(ES2015)吧！`babel-core` 是Babel编译器的核心。
安装：`npm install --save-dev babel-core`

---

####[babel-loader](https://www.npmjs.com/package/babel-loader) [必需]
> loader 用于转换应用程序的资源文件，他们是运行在nodejs下的函数，
使用参数来获取一个资源的来源并且返回一个新的来源针对webpack的babel加载器。
`babel-loader` 就是告诉webpack去加载我们写的使用了es6语法的js文件。
安装：`npm install --save-dev babel-loader`

---

#### [babel-runtime](http://babeljs.io/docs/plugins/transform-runtime/#why) [强烈推荐]
> Babel默认只转换新的JavaScript语法，而不是转换新的API，
比如Iterator、Generator、Set、Maps、Proxy、Reflect，Symbol、Promise等全局对象，
以及一些定义在全局对象上的方法（比如Object.assign）都不会转码。
举例来说，ES6在Array对象上新增了Array.from方法。
[延伸阅读,强烈推荐](https://segmentfault.com/a/1190000006930013?utm_source=tuicool&utm_medium=referral)
安装：`npm install --save babel-runtime`

#### [babel-plugin-transform-runtime](http://babeljs.io/docs/plugins/transform-runtime/#why) [开发需要]
> 和上面的 `babel-runtime` 搭配使用
安装：`npm install --save-dev babel-plugin-transform-runtime`

####[babel-preset-latest](http://babeljs.io/docs/plugins/preset-latest/) [必需]
> es2015,es2016,es2017转码规则。为所有es6插件所设置的babel预设，
有了它，诸如，es6的箭头函数，类，等等语法特性才能向es5转换。
安装：`npm install --save-dev babel-preset-latest`

---

####[babel-preset-react](https://github.com/babel/babel) [必需]
> react转码规则。为所有react插件所设置的babel预设。有了它，才能识别转译jsx语法等。
安装：`npm install --save-dev babel-preset-react`

---

####[react-hot-loader](https://www.npmjs.com/package/react-hot-loader) [开发需要]
> 可以使react组件在浏览器上实时更新而无需手动刷新。
安装：`npm install --save-dev react-hot-loader@3.0.0-beta.6`
备注：用的是3.0最新版本，这版本很强大。

---

####[babel-preset-stage-X](https://www.npmjs.com/package/babel-preset-stage-0) [必需]
> ES7不同阶段语法提案的转码规则（共有4个阶段），选装**一个**
在进行实际开发时，可以根据需要来设置对应的stage。如果省事懒得折腾，一般设置为stage-0即可。
npm install --save-dev babel-preset-stage-0
npm install --save-dev babel-preset-stage-1
npm install --save-dev babel-preset-stage-2
npm install --save-dev babel-preset-stage-3
[stage-X详解](http://www.cnblogs.com/flyingzl/p/5501247.html)

---

####[redbox-react](https://github.com/KeywordBrain/redbox-react) [开发需要]
> 这个插件将会以一个非常优雅的方式（看demo演示）将你的错误呈现在页面上，这样就省去了查看console.log的麻烦；

---

####[html-webpack-plugin](https://www.npmjs.com/package/html-webpack-plugin) [小工具]
> 一个服务于webpack打包资源的简易的HTML文件生成器,它可以动态生成HTML
之所以要动态生成，主要是希望webpack在完成前端资源打包以后，自动将打包后的资源路径和版本号写入HTML中，达到自动化的效果
安装：`npm install --save-dev html-webpack-plugin`

---

####[express](https://www.npmjs.com/package/express) [开发需要]
> 基于 Node.js 平台，快速、开放、极简的 web 开发框架。
在这里用于配置开发服务器。
安装：`npm install --save-dev express`

-
## [babel-preset-es2015-webpack](https://github.com/gajus/babel-preset-es2015-webpack#babel-preset-es2015-webpack)

## [better-npm-run](https://www.npmjs.com/package/better-npm-run)
better-npm-run：可以设置windows与linux通用的启动环境

### 安装包及更改配置

和其他包一样还是老方法安装

npm install --save-dev better-npm-run
更改package.json配置
```js
 ...
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "build": "webpack --verbose --color --display-error-details --config ./config/webpack.dev.js ",
    "start-dev": "better-npm-run start-dev"
  },
  "betterScripts":{
    "start-dev":{
      "command":"babel-node ./src/server.js",
      "env":{
        "NODE_ENV":"development"
      }
    }
  },
...
```
启命令变为npm run start-dev
## [concurrently](https://www.npmjs.com/package/concurrently)
concurrently：支持多命令同时启动
